#
#       Property of PATWIC AB -- Programming and the world it Creates
#       All Rights reserved.
#

# A boolean variable deciding if the player is still alive
alive = True

poem = """you can whistle a merry tune in a flurry,
while the snore or barf of a turlingdrome hurry.
if a goblin you kiss,
it will angrily hiss.
And something green must bite you in worry.
""" 

print("The worst poem in the world. Ahem..".center(60))
print(poem)

print("You enter a dark room with two doors.  Do you go through door #1 or door #2?")

# Ask what door the player takes
door = input("> ")

if door == "1":
    bear_angry = True

    while bear_angry:
        print("There's a giant bear here eating a cheese cake. What do you do?")
        print("1. Take the cake.")
        print("2. Scream at the bear.")
        print("Improvise...")
        action = input("> ")

        # Important information about how to escape bears
        stuff_that_scares_bears = "whistle barf bite snore kiss flurry"
        
        if "1" in action:
            print("The bear growls menacingly. You've changed your mind.")
        elif "2" in action:
            print("The bear eats your legs off.  Good luck finding them!")
        elif action in stuff_that_scares_bears:
            print("Well, doing " + action + " seems to work. Bear runs away.")
            bear_angry = False
        else:
            print("The bear watches you " + action + " for a minute, shakes its head and eats more cheese cake.")


elif door == "2":
    print("You stare into the endless abyss of Cthulhu's retina.")
    print("You have already lost your mind, will you save your life?")
    print("1. Blueberries.")
    print("2. Will I, Won't I fiddle with very small rocks?")
    print("3. I suddenly understand that Justin Bieber is a GENIUS!!!")

    insanity = input("> ")

    if insanity == "1" or insanity == "2":
        print("Your body survives powered by a mind of jello. Good job!")
    else:
        print("The insanity rots your eyes into a pool of muck.")
        print("You are very dead, but you are also much to insane to notice.")
        alive = False


else:
    print("You stumble around and fall on a potato peeler and die. Good job!")

# Create a third room that the USER has to go through.
# YOU decide what will happen!

# print some stuff...


# ask the user for action


# decide what to do



# When the USER leaves your room he she it whatever will end up here. 


if alive:
    print("You are still alive and you notice the exit to your left.")
    print("If you are lucky, you are having a slice of delicious cake.")
    print("Are you ready to take the exit? ")

    exit = input("> ")

    # Create code that forces the user to say Yes.

    print("Sorry, I didn't listen. Anyway. You can't go. Not just yet.")
    print("You HAVE to guess a number.")

else:
    print("I'm sorry you're dead. If you ever want to come back, well.... ")
    print("You HAVE to guess a number.")
    

    # Create a guess game at the end of the program. Make sure to be very annoying.

